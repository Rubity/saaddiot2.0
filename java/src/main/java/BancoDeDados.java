import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//Classe responsável por fazer a inserção dos valores no banco de dados.
public class BancoDeDados {

    //Variável que recebe as informações do local onde esta hospedado o banco de dados.
    Connection conn;

    //Construtor da classe BancoDeDados.
    public BancoDeDados() {
        try {
            conn = DriverManager.getConnection("jdbc:mariadb://localhost:3306/Sensores", "root", "inmetro");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Metodo responsável por realizar a inserção de dados no banco de dados
    public void inserirValor(String dados, int grandeza) {

            try {
                Statement stmt = conn.createStatement();
                String sql = "";

                //Define, através da variável de controle 'grandeza', em qual tabela e coluna o dado será inserido.
                switch (grandeza) {
                    case 1:
                        sql = "INSERT INTO SensorLuz (ValorLuz) VALUES(" + dados + ")";
                        break;

                    case 2:
                        sql = "INSERT INTO SensorProximidade (ValorDistancia) VALUES(" + dados + ")";

                        break;

                    case 3:
                        sql = "INSERT INTO SensorTemperatura (ValorTemperatura) VALUES(" + dados + ")";
                        break;

                }
                //Realiza a inserção na tabela determinada.
                stmt.executeUpdate(sql);
            }
            catch (SQLException e) {
                System.out.println("Erro ao adicionar um valor na coluna");
            }
        }
    }
