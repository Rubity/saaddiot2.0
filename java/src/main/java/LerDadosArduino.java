import java.util.Scanner;

import com.fazecast.jSerialComm.SerialPort;

//Clase responsável por receber e enviar dados para o Arduino.
public class LerDadosArduino {

    //Recebe a porta pela qual o Arduino esta conectado.
    private static final String PORT = "ttyACM0";

    //Recebe as informações do Arduino.
    private static Scanner scannerIn;

    //Recebe a informacão se a porta esta conectada ou não.
    private static SerialPort chosenPort;

    //Construtor da classe LerDadosArduino.
    public LerDadosArduino(){
        abrirPorta();
    }

    //Diz se esta ou não conectado na porta do Arduino.
    public boolean portaEstaAberta() {
        return chosenPort.isOpen();
    }

    //Define parametros da conexão como por exemplo a velocidade da porta serial.
    public void abrirPorta() {
        chosenPort = SerialPort.getCommPort(PORT);
        chosenPort.setBaudRate(9600);
        chosenPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
        chosenPort.openPort();

        //Faz uma verificacão para saber se o programa esta conectado ao Arduino.
        if(chosenPort.openPort()){
            //Recebe o fluxo de informações que vem da porta serial
            scannerIn = new Scanner(chosenPort.getInputStream());
        }else{
            System.out.println("Erro ao conectar ao Arduino");
        }
    }

    //Enquanto o Arduino mandar informacão, faz a leitura do scannerIn e retorna o valor na String.
    public String lerPortaSerial() {
        try{
            return scannerIn.nextLine();
        }catch(NullPointerException e){
            e.printStackTrace();
        }
        return "Houston, we have a problem!!";
    }
}