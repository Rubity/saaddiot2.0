import java.sql.SQLException;

public class Main {
//Classe principal, responsável por chamar as outras classes.

    public static void main(String[] args) throws SQLException{

        //Instanciando objetos das outras classes.
        LerDadosArduino lerDadosArduino = new LerDadosArduino();
        Medidor medidor = new Medidor();
        BancoDeDados bancoDeDados = new BancoDeDados();

        // Estrutura de código responsável por manter o programa em um loop infinito.
        while (true) {
            //Chama um método de outra classe, responsável pela leitura dos dados enviados pelo Arduino.
            medidor.tratamentoDados(lerDadosArduino.lerPortaSerial());

            //Tem com funcao chamar o método da classe BancoDeDados.
            bancoDeDados.inserirValor(medidor.getLumi(), 1);
            bancoDeDados.inserirValor(medidor.getCont(), 2);
            bancoDeDados.inserirValor(medidor.getTemp(), 3);
        }
    }
}