import java.util.Arrays;

//Clase responsável por fazer o tratamento da String recebida pelo Arduino
public class Medidor {

    //Variáveis para receber os valores das medições.
    public String Cont;
    public String Temp;
    public String Lumi;

    //Getter e Setter das variáveis.
    public String getCont() {
        return String.valueOf(Cont);
    }

    public void setCont(String Dist){
        this.Cont = (Dist); }

    public String getTemp () {
        return String.valueOf(Temp);
    }

    public void setTemp (String Temp){
        this.Temp = (Temp);
    }

    public String getLumi () {
        return String.valueOf(Lumi);
    }

    public void setLumi (String Lumi){
        this.Lumi = (Lumi);
    }

    //Separa a string, atribuindo cada parte a sua respectiva variável.
    public void tratamentoDados(String readDataBySerialPort) {
        try {
            String[] textoSeparado = readDataBySerialPort.split(";|;\\s");

            System.out.println(Arrays.toString(textoSeparado));

            setTemp(textoSeparado[0]);

            setLumi(textoSeparado[1]);

            setCont(textoSeparado[2]);

            System.out.println("Luminosidade: " + getLumi() + "% - Contador: " + getCont() + " - Temperatura: " + getTemp());
            System.out.println("");

        }catch(Exception e){
            System.err.println("Leitura ignorada, string incompleta");
        }
    }

}

