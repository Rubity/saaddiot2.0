//Realiza a importação de algumas bibliotecas para o projeto.
#include <RTClib.h>
#include <Ultrasonic.h>
#include <LiquidCrystal.h>
#include <Wire.h>

//Define os pinos para o trigger e echo do sensor de proximidade.
#define pino_trigger 6
#define pino_echo 7

#define porta_rele 8 //Define o pino para o rele.
#define Alarme 13 //Define o pino para o buzzer.
#define LM35 A1 //Define o pino que ira ler a saida do LM35.
#define ldrPin A5 //Define o pino que ira ler a saida do LDR.

RTC_Millis rtc; //Variavel para medição do tempo.

int minutoAgora, horaAgora; //Variaveis que recebem respectivamente o minuto e a hora do respectivo momento.
int tempo = 300; //Delay pradrao para ser utilizado no programa.
float contador = 0; //Recebe o numero de pessoas presentes no laboratorio.
String temp, luz, cont, dados = ""; // Variaveis do tipo String que receberao os valores dos sensores.

Ultrasonic ultrasonic(pino_trigger, pino_echo); //Inicializa o sensor nos pinos definidos acima

LiquidCrystal lcd(12, 11, 5, 4, 3, 2); //Define os pinos que serão utilizados para ligação ao display

//Função que só será realizada uma vez.
void setup()
{
  //Define a velocidade e inicializa a comunicação da porta serial.
  Serial.begin(9600);

  //Define o número de colunas e linhas do LCD
  lcd.begin(16, 2);

  //Define a porta rele e o buzzer como OUTPUT.
  pinMode(porta_rele, OUTPUT);
  pinMode(Alarme, OUTPUT);

  //Inicia o DateTime.
  rtc.begin(DateTime(__DATE__, __TIME__));
}

//Função que ira ficar executando enquanto o programa estiver rodando.
void loop() {
  horaAtual();
  luz  =  SensorLuz();
  temp = SensorTemperatura();
  cont = SensorProximidade();
  dados = concatenacao();

  Serial.println(dados);
  dados = "";

}

//Funcao que define a quantidade de pessoas presentes no laboratorio.
String SensorProximidade() {

  long microsec = ultrasonic.timing(); //Retorna o tempo demorado para o sinal ultrassonico sair e retorna ao sensor
  float  cmMsec = ultrasonic.convert(microsec, Ultrasonic::CM); //Realiza a conversao de unidade de tempo para unidade de comprimento

  float D1 = cmMsec;
  delay(tempo);

  microsec = ultrasonic.timing();
  cmMsec = ultrasonic.convert(microsec, Ultrasonic::CM);

  float D2 = cmMsec;
  delay(tempo);

  microsec = ultrasonic.timing();
  cmMsec = ultrasonic.convert(microsec, Ultrasonic::CM);

  float D3 = cmMsec;
  delay(tempo);

  microsec = ultrasonic.timing();
  cmMsec = ultrasonic.convert(microsec, Ultrasonic::CM);

  //Faz o delta comprimento das medicoes.
  float dD1 = D2 - D1;
  float dD2 = D3 - D2;

  //Define se a pessoa esta entrando ou saindo.
  if ((dD1 > 5) && (dD2 > 5)) {
    contador++;
  } else if ((dD1 < -5) && (dD2 < -5) && (contador > 0)) {
    contador--;
  }

  //Acende ou apaga as luzes, dependendo da quantidade de pessoas no laboratório.
  if (contador > 0 ) {
    digitalWrite(porta_rele,  LOW);
  } else {
    digitalWrite(porta_rele,  HIGH);
  }

  //Define a posicao e imprime no lcd a variavel contador, que tem como valor o numero de pessoas no laboratorio.
  lcd.setCursor(14, 0);
  lcd.print(contador);

  return String(contador);
}

//Funcao que inicia a sirene no buzzer.
void alarmeBuzzer()
{
  //Simula uma sirene.
  int i = 0;
  while (i < 1000) {
    for (int x = 0; x < 180; x++) {
      //converte graus para radiando e depois obtém o valor do seno
      float seno = (sin(x * 3.1416 / 180));
      //gera uma frequência a partir do valor do seno
      float frequencia = 2000 + (int(seno * 1000));
      tone(Alarme, frequencia, tempo);
      delay(2);
      i++;
    }
    noTone(Alarme);
  }
}

//Funcao que retorna a luminosidade no ambiente.
String SensorLuz() {
  float luminosidade = 0;
  //Pega o valor da ddp medido no sensor ldr.

  //for (int i = 0; i < 5 ; i++) {
    luminosidade = analogRead(ldrPin);
   // luminosidade +=luminosidade;
  //}
  
  //Define a porcentagem de luminosidade no ambiente.
  float ldrValor = luminosidade / 1023 * 100;

    //Posiciona o cursor na coluna 0, linha 0;
  lcd.setCursor(0, 0);
  //Envia o texto entre aspas para o LCD
  lcd.print("Luz:");
  //Envia o valor de luminosidade para o LCD
  lcd.print(ldrValor);

  lcd.setCursor(10, 0);
  lcd.print("%");

  return String(ldrValor);
}

//Funcao que retorna a temperatura no ambiente em que o sensor está inserido.
String SensorTemperatura() {
  float temperatura = 0;
  //Recebe o valor da temperatura no ambiente

  //Seleciona a posicao e imprime no lcd "temp:" e o valor da temperatura.

  for (int i = 0; i < 5 ; i++) {
    
    analogRead(LM35);
    delay(10);
    temperatura += analogRead(LM35) * 0.54;
  }

  temperatura = temperatura/5;

  //Seleciona a posicao e imprime no lcd "temp:" e o valor da temperatura.
  lcd.setCursor(0, 1);
  lcd.print("Temp:");
  lcd.print(temperatura);

  temp = String ( temperatura) ;

  return String(temperatura);
}

//Funcao que concatena as Strings.
String concatenacao() {
  dados += temp;
  dados += ";";
  dados += luz;
  dados += ";";
  dados += cont;

  return String(dados);
}

//Funcao que retorna a hora e o minuto do respectivo momento.
void horaAtual() {
  //Pega o tempo do respectivo momento.
  DateTime now = rtc.now();

  //Recebe a a respectiva hora.
  horaAgora = now.hour();
  //Recebe o respectivo minuto.
  minutoAgora = now.minute();

  //Denpendendo do horario e da quantidade de pessoas no laboratorio, aciona o alarme.
  if ((contador > 0 && horaAgora >= 17 && minutoAgora >= 30) || (contador > 0 && horaAgora <= 7)) {
    alarmeBuzzer();
  }
}


